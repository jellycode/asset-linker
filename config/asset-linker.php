<?php

return [

    'use_cdn' => env('ASSET_LINKER_USE_CDN', false),

    'cdn_url' => '',

    'manifest_dir' => '',

    'manifest_name' => '',

    'filesystem' => [
        'disk' => 'asset-cdn',

        'options' => [
            //
        ],
    ],

];
