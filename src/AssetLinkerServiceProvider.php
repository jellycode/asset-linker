<?php

namespace Jellycode\AssetLinker;

use Illuminate\Support\ServiceProvider;

class AssetLinkerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {        
      $this->publishes([
          __DIR__.'/../config/asset-linker.php' => config_path('asset-linker.php'),
      ], 'config');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/asset-linker.php', 'asset-linker');
    }
}
