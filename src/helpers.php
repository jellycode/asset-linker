<?php

use Illuminate\Support\Str;
use Illuminate\Support\HtmlString;
use Jellycode\AssetLinker\Util\Files;
use Jellycode\AssetLinker\Util\Tags;

if (! function_exists('asset_manifest_linker')) {
  
    function asset_manifest_linker($path)
    {

      $tags = collect();

      $use_cdn = config('asset-linker.use_cdn');
      $cdn_url = config('asset-linker.cdn_url');
      $manifest_dir = config('asset-linker.manifest_dir');
      $manifest_name = config('asset-linker.manifest_name');

      $filesClass = new Files();
      $tagsClass = new Tags();

      if (! Str::startsWith($path, '/')) {
          $path = "/{$path}";
      }

      $extension = pathinfo($path, PATHINFO_EXTENSION);

      $assets = $filesClass->getAssets($extension);

      $tags = $tagsClass->createTags($assets, $extension);

      $tagstring = $tags->implode(' ');

      return $tags->implode(' ');

    }
}

if (! function_exists('asset_linker')) {

    /**
     * Generate an asset path for the application.
     *
     * @param  string  $path
     * @return string
     */
    function asset_linker($path)
    {
        if (! config('asset-linker.use_cdn')) {
            return asset($path);
        }

        $cdnUrl = config('asset-linker.cdn_url');
        // Remove slashes from ending of the path
        $cdnUrl = rtrim($cdnUrl, '/');

        return $cdnUrl.'/'.trim($path, '/');
    }
}
