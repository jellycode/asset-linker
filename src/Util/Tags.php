<?

namespace Jellycode\AssetLinker\Util;

use Illuminate\Support\Str;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Facades\File;
use Jellycode\AssetLinker\Util\Files;
use Exception;

class Tags
{

  public function createTags($assets, $type) {

    $tagstring = collect();
    $prefix = "";
    $fileClass = new Files();

    $manifest_dir = config('asset-linker.manifest_dir');

    if (config('asset-linker.use_cdn')) {
      $prefix = config('asset-linker.cdn_url');
    }

    if ($type == 'css') {

      $assets->each(function($item, $key) use($prefix,$manifest_dir,$tagstring,$fileClass) {

        if (strstr($item, 'critical')) {

          $style = $fileClass->getFile($manifest_dir, $item);
          $tagstring->prepend('<style>' . $style . '</style>');

        } else {
          $file_url = $prefix . "/" . $manifest_dir . "/" . $item;
          $tagstring->prepend('<link rel="preload" href="' . $file_url . '" as="style" onload="this.onload=null;this.rel=\'stylesheet\'"><noscript><link rel="stylesheet" href="' . $file_url . '"></noscript>');
          //$tagstring->prepend('<link href="' . $file_url . '" rel="stylesheet">');

        }

      });

    }

    if ($type == 'js') {

      $filesClass = new Files();

      $filearray = $assets->toArray();
      $files = $filearray;

      usort($files, function($a, $b) use($filesClass) {
        $pos_a = $filesClass->getAssetValue($a);
        $pos_b = $filesClass->getAssetValue($b);
        
        return $pos_a <=> $pos_b;
      });

      if (!$filesClass->isHot()) {

        $assets = collect($files)->reverse();
        
        $assets->each(function($item, $key) use($prefix,$manifest_dir,$tagstring) {
  
          $file_url = $prefix . "/" . $manifest_dir . "/" . $item;
          $tagstring->prepend('<script src="' . $file_url . '"></script>');
  
        });
  
      } else {
  
        $tagstring->prepend('<script src="//localhost:8080/webpack/app.js"></script>');
  
      }

    }

    return $tagstring;

  }

}