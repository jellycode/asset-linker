<?

namespace Jellycode\AssetLinker\Util;

use Illuminate\Support\Str;
use Illuminate\Support\HtmlString;
use Illuminate\Support\Facades\File;
use Exception;

class Files {

  public function getFile($dir, $name) {
    

    if ($dir && ! Str::startsWith($dir, '/')) {
      $dir = "/{$dir}";
    }
    $dir = public_path($dir.'/'.$name);

    if (! file_exists($dir)) {
      throw new Exception('The file does not exist.');
    }
    $contents = File::get($dir);
    if ($this->isJson($contents)) {
      $contents = json_decode($contents, true);
      return collect($contents);
    }
    return $contents;
  }

  public function getAssets($type) {

    if ($type == 'css') {

      if ($this->isHot()) {

        return collect();

      }

    }

    $manifest_dir = config('asset-linker.manifest_dir');
    $manifest_name = config('asset-linker.manifest_name');
    $manifest = $this->getFile($manifest_dir, $manifest_name);

    $files = array();
    $flat = $manifest->flatten();
    $filtered = $flat->filter(function($item) use ($type) {
        return preg_match('/^.*\.('.$type.')$/i', $item);
    });

    $filtered = $filtered->values();

    return $filtered;

  }

  public function getAssetValue($file) {

    switch ($file) {
      case stristr($file, 'manifest'):
        return 1;
      case stristr($file, 'vendor'):
        return 2;
      case stristr($file, 'app'):
        return 3;
    }

  }

  public function isHot() {

    $hot_path = base_path('hot');
    return file_exists($hot_path);

  }

  private function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
   }

}